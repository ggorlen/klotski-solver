"""
Klotski solver

TODO
- determine why mirroring doesn't appear to be helping
- consider skipping or changing DFS for move generation--likely identical positions are being re-generated on each step (could hash them)
  - use DFS to get the true shortest path for daughter (81)
- refactor to avoid making copies, just keep track of which move was made and do the opposite move to backtrack (actually not so easy?)

python -m cProfile klotski.py 

References:
- http://www.pvv.ntnu.no/~spaans/spec-cs.pdf
- https://cs.lmu.edu/~ray/notes/backtracking/
- https://stackoverflow.com/questions/42318343/avoid-duplicates-in-n-queen-iterative-solutions-no-recursion-allowed
- https://stackoverflow.com/questions/2909106/whats-a-correct-and-good-way-to-implement-hash
- https://www.asmeurer.com/blog/posts/what-happens-when-you-mess-with-hashing-in-python/
- https://hynek.me/articles/hashes-and-equality/
"""

from collections import deque
from math import ceil


class Block:
    def __init__(self):
        self.squares = ()
        self.is_goal = False
         
    def __eq__(self, other):
        return self.squares == other.squares and self.is_goal == other.is_goal

    def __hash__(self):
        return hash(self.squares) # hash(tuple([self.squares, self.is_goal]))

    def __lt__(self, other):
        return self.squares < other.squares

    def can_move(self, direction, grid, w, h):
        for x, y in self.squares:
            new_loc = x + direction[0], y + direction[1]

            if new_loc[1] >= h or new_loc[1] < 0 or \
               new_loc[0] >= w or new_loc[0] < 0 or \
               grid[new_loc[1]][new_loc[0]] and \
               new_loc not in self.squares:
                return False

        return True

    def move(self, direction, grid, w, h):
        if not self.can_move(direction, grid, w, h):
            return False

        new_locations = []

        for x, y in self.squares:
            new_locations.append((x + direction[0], y + direction[1]))
            grid[y][x] = 0

        self.squares = tuple(new_locations)

        for x, y in self.squares:
            grid[y][x] = 1
        
        return True


class Board:
    adjacent = ((-1, 0), (1, 0), (0, 1), (0, -1))

    def __init__(self):
        pass

    def create(self, data, goal_squares):
        self.goal_squares = goal_squares
        self.h = len(data)
        self.w = len(data[0])
        self.blocks = []
        self.grid = [[0] * self.w for y in range(self.h)]
        seen = {}

        for y, row in enumerate(data):
            for x, cell in enumerate(row):
                if cell != " ":
                    if cell not in seen:
                        seen[cell] = Block()
                        self.blocks.append(seen[cell])

                        if cell == "@":
                            seen[cell].is_goal = True
                            self.goal_block = seen[cell]
                    
                    seen[cell].squares += ((x, y),)
                    self.grid[y][x] = 1

        self.sorted_blocks = tuple(sorted(self.blocks))
        return self

    def horizontal_reflection(self):
        cpy = Board()
        cpy.h = self.h
        cpy.w = self.w
        cpy.goal_squares = self.goal_squares
        cpy.grid = [x[:] for x in self.grid]
        cpy.blocks = [Block() for b in self.blocks]

        for dest, src in zip(cpy.blocks, self.blocks):
            dest.squares = tuple((cpy.w - x - 1, y) for x, y in src.squares)
            dest.is_goal = src.is_goal

            if dest.is_goal:
                cpy.goal_block = dest

        return cpy

    def copy(self):
        cpy = Board()
        cpy.h = self.h
        cpy.w = self.w
        cpy.goal_squares = self.goal_squares
        cpy.grid = [x[:] for x in self.grid]
        cpy.blocks = [Block() for b in self.blocks]

        for dest, src in zip(cpy.blocks, self.blocks):
            dest.squares = src.squares[:]
            dest.is_goal = src.is_goal

            if dest.is_goal:
                cpy.goal_block = dest

        cpy.sorted_blocks = self.sorted_blocks[:]
        return cpy
        
    def move(self, direction, block_idx):
        self.blocks[block_idx].move(direction, self.grid, self.w, self.h)
        self.sorted_blocks = tuple(sorted(self.blocks))

    def get_moves(self):
        moves = [[] for _ in self.blocks]

        for i, block in enumerate(self.blocks):
            for direction in Board.adjacent:
                if block.can_move(direction, self.grid, self.w, self.h):
                    moves[i].append(self.copy())
                    moves[i][-1].move(direction, i)
            
        return moves
    
    def solve(self):
        visited = {self: None}
        queue = deque([self])

        while queue:
            curr = queue.popleft()

            if curr.solved():
                path = []
                step = curr

                while step:
                    path.append(step)
                    step = visited[step]

                return path[::-1]

            for block in curr.get_moves():
                for moved in block:
                    if moved not in visited:
                        visited[moved] = curr
                        queue.append(moved)
    
    def solved(self):
        return self.goal_block.squares == self.goal_squares

    def __eq__(self, other):
        return self.sorted_blocks == other.sorted_blocks
         
    def __hash__(self):
        return hash(self.sorted_blocks)

    def __str__(self):
        res = [[" " for x in range(self.w)] for y in range(self.h)]
        counter = 97

        for block in self.blocks:
            if block.is_goal:
                for square in block.squares:
                    res[square[1]][square[0]] = "@"
            else:
                for square in block.squares:
                    res[square[1]][square[0]] = chr(counter)
                
                counter += 1    

        return "\n " + "-" * (self.w + 2) + "\n |" + \
               "|\n |".join(["".join(map(str, row)) for row in res]) + \
               "|\n " + "-" * (self.w // 2) + "  " + "-" * (self.w // 2) + "\n"


if __name__ == "__main__":
    daughter = [
        "0@@1",
        "0@@1",
        "2334",
        "2564",
        "7  8"
    ]
    pennant = [
        "@@11",
        "@@22",
        "34  ",
        "5677",
        "5688"
    ]
    goal_squares = ((1, 3), (2, 3), (1, 4), (2, 4))
    klotski = Board().create(daughter, goal_squares)
    print(klotski)

    with open("result.txt", "w") as f:
        soln = klotski.solve()
        f.writelines(list(map(str, soln)) + ["%d steps" % len(soln)])

    # testing hashing TODO
    #print([b.squares for b in klotski.blocks])
    #print(klotski.grid)
    #c = klotski.copy()
    #klotski.blocks[-1].move((-1, 0), klotski.grid, 4, 5)
    #print(klotski)
    #print([b.squares for b in klotski.blocks])
    #print(klotski.grid)
    #print(hash(c))
    #print(hash(klotski))
    #klotski.blocks[6].move((1, 0), klotski.grid, 4, 5)
    #klotski.blocks[6].move((0, -1), klotski.grid, 4, 5)
    #klotski.blocks[6].move((1, 0), klotski.grid, 4, 5)
    #klotski.blocks[-1].move((-1, 0), klotski.grid, 4, 5)
    #klotski.blocks[6].move((0, 1), klotski.grid, 4, 5)
    #klotski.blocks[6].move((1, 0), klotski.grid, 4, 5)
    #klotski.blocks[-1].move((-1, 0), klotski.grid, 4, 5)
    #print(c)
    #print(c.horizontal_reflection(), "<")
    #print(klotski)
    #print(klotski.horizontal_reflection(), "<")
    #print(hash(c), [x.squares for x in c.blocks])
    #print(hash(klotski), [x.squares for x in klotski.blocks])
    #s = set()
    #s.add(c)
    #s.add(klotski)
    #print(len(s))
    #print(c == klotski)
